"""
Your awesome Distance Vector router for CS 168
"""

import sim.api as api
import sim.basics as basics


# We define infinity as a distance of 16.
INFINITY = 16


class DVRouter (basics.DVRouterBase):
  #NO_LOG = True # Set to True on an instance to disable its logging
  #POISON_MODE = True # Can override POISON_MODE here
  #DEFAULT_TIMER_INTERVAL = 5 # Can override this yourself for testing

  def __init__ (self):
    """
    Called when the instance is initialized.

    You probably want to do some additional initialization here.
    """
    self.start_timer() # Starts calling handle_timer() at correct rate
    self.table = {} # host_dst : {'cost' : 1, 'nextPort' : 1, 'time' : 123}
    self.hostPort = set() # host port
    self.neighbors = {} # port : cost
    # self.POISON_MODE = True

  def handle_link_up (self, port, latency):
    """
    Called by the framework when a link attached to this Entity goes up.

    The port attached to the link and the link latency are passed in.
    """
    self.neighbors[port] = latency
    for dst, val in self.table.items():
      self.send(basics.RoutePacket(dst, val['cost']), port)

  def handle_link_down (self, port):
    """
    Called by the framework when a link attached to this Entity does down.

    The port number used by the link is passed in.
    """
    if port in self.hostPort:
      self.hostPort.remove(port)
    if port in self.neighbors:
      del self.neighbors[port]
    for dst, val in self.table.items():
      if val['nextPort'] == port:
        del self.table[dst]
        if self.POISON_MODE:
          self.send(basics.RoutePacket(dst, INFINITY), flood=True)

  def handle_rx (self, packet, port):
    """
    Called by the framework when this Entity receives a packet.

    packet is a Packet (or subclass).
    port is the port number it arrived on.

    You definitely want to fill this in.
    """
    #self.log("RX %s on %s (%s)", packet, port, api.current_time())
    if isinstance(packet, basics.RoutePacket):
      self.routeUpdate(packet, port)
    elif isinstance(packet, basics.HostDiscoveryPacket):
      self.discoveryUpdate(packet, port)
    else:
      # Totally wrong behavior for the sake of demonstration only: send
      # the packet back to where it came from!
      if packet.dst in self.table:
        nextPort = self.table[packet.dst]['nextPort']
        if nextPort != port and self.table[packet.dst]['cost'] != INFINITY:
          self.send(packet, nextPort)

  def routeUpdate(self, packet, port):
    newDest = packet.destination
    costToDest = packet.latency + self.neighbors[port]
    if packet.latency >= INFINITY:
      if newDest in self.table and self.table[newDest]['nextPort'] == port:
        del self.table[newDest]
        if self.POISON_MODE:
          newPacket = basics.RoutePacket(newDest, INFINITY)
          self.send(newPacket, port, flood=True)
    elif newDest in self.table:
      oldCost = self.table[newDest]['cost']
      if costToDest == oldCost and self.table[newDest]['nextPort'] == port:
        self.table[newDest]['time'] = api.current_time()
      elif costToDest < oldCost or self.table[newDest]['nextPort'] == port:
        self.table[newDest] = {'cost' : costToDest, 'nextPort' : port, 'time' : api.current_time()}
        newPacket = basics.RoutePacket(newDest, costToDest)
        self.send(newPacket, port, flood=True)
        if self.POISON_MODE:
          self.send(basics.RoutePacket(newDest, INFINITY), port)
    else:
      self.table[newDest] = {'cost' : costToDest, 'nextPort' : port, 'time' : api.current_time()}
      newPacket = basics.RoutePacket(newDest, costToDest)
      self.send(newPacket, port, flood=True)
      if self.POISON_MODE:
        self.send(basics.RoutePacket(newDest, INFINITY), port)

  def discoveryUpdate(self, packet, port):
    self.hostPort.add(port)
    newHost = packet.src
    costToHost = self.neighbors[port]
    if newHost in self.table:
      oldCost = self.table[newHost]['cost']
      if costToHost < oldCost:
        self.hostTableUpdate(newHost, costToHost, port)
      elif costToHost == oldCost:
        self.table[newHost]['time'] = api.current_time()
      else:
        newPacket = basics.RoutePacket(newHost, costToHost)
        self.send(newPacket, port, flood=True)
    else:
      self.hostTableUpdate(newHost, costToHost, port)

  def hostTableUpdate(self, host, newCost, port):
    self.table[host] = {'cost' : newCost, 'nextPort' : port, 'time' : api.current_time()}
    newPacket = basics.RoutePacket(host, newCost)
    self.send(newPacket, port, flood=True)

  def check_time(self, t):
    cur = api.current_time()
    diff = abs(cur - t);
    if (diff >= 15):
      return False
    else:
      return True

  def handle_timer (self):
    """
    Called periodically.

    When called, your router should send tables to neighbors.  It also might
    not be a bad place to check for whether any entries have expired.
    """
    for host, val in self.table.items():
      nextPort = val['nextPort']
      if self.check_time(val['time']):
        self.send(basics.RoutePacket(host, val['cost']), nextPort, flood=True)
      else:
        if nextPort in self.hostPort:
          self.send(basics.RoutePacket(host, val['cost']), nextPort, flood=True)
        else:
          del self.table[host]
          if self.POISON_MODE:
            self.send(basics.RoutePacket(host, INFINITY), nextPort, flood=True)
