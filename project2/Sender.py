import sys
import getopt

import Checksum
import BasicSender

'''
This is a skeleton sender class. Create a fantastic transport protocol here.
'''
class Sender(BasicSender.BasicSender):
    def __init__(self, dest, port, filename, debug=False, sackMode=False):
        super(Sender, self).__init__(dest, port, filename, debug)
        self.sackMode = sackMode
        self.debug = debug
        self.chunk_size = 1472
        self.seq = 0
        self.timeout = 0.5
        self.window = 7
        self.sent = 0
        self.seq_dup = {} # seq : number of occurs
        self.fast_retransmit_rate = 4
        self.sack_set = set()

    # Main sending loop.
    def start(self):
        # add things here
        self.send_msg("syn", self.seq)
        self.send_dat_read_file()
        self.send_msg("fin", self.seq)

    def send_msg(self, msg_type, seq, data=""):
        msg = self.make_packet(msg_type, seq, data)
        self.send(msg)
        ack = self.receive(self.timeout)
        if ack == None or (not Checksum.validate_checksum(ack)):
            self.send_msg(msg_type, seq)
        else:
            if self.sackMode:
                self.parse_sack(ack)
            self.seq = self.get_seq_from_ack(ack)

    def fast_retransmit(self, ack):
        new_seq = self.get_seq_from_ack(ack)
        if new_seq in self.seq_dup:
            self.seq_dup[new_seq] += 1
        else:
            self.seq_dup[new_seq] = 1

    def send_dat_read_file(self):
        self.send_dat_window()
        while True:
            if self.sent == 0:
                break
            for _ in range(self.sent):
                ack = self.receive(self.timeout)
                if ack == None or (not Checksum.validate_checksum(ack)):
                    continue
                else:
                    if self.sackMode:
                        self.parse_sack(ack)
                    self.fast_retransmit(ack)
                    self.seq = self.get_seq_from_ack(ack)
            if self.seq in self.seq_dup and self.seq_dup[self.seq] >= self.fast_retransmit_rate:
                msg = self.get_data_packet(self.seq)
                self.send(msg)
                ack = self.receive(self.timeout)
                if ack != None and (Checksum.validate_checksum(ack)):
                    if self.sackMode:
                        self.parse_sack(ack)
                    self.seq = self.get_seq_from_ack(ack)
            if self.sackMode:
                self.send_dat_window(self.sack_set)
            else:
                self.send_dat_window()

    def send_dat_window(self, exclude=set()):
        self.sent = 0
        for i in range(self.window):
            seq = self.seq + i
            if self.sackMode and seq in exclude:
                continue
            dat = self.get_data_packet(seq)
            if dat == None:
                break
            self.send(dat)
            self.sent += 1

    def parse_sack(self, sack_msg):
        ack = self.split_packet(sack_msg)[1]
        sacks = ack.split(';')[1]
        sacks = sacks.split(',')
        for s in sacks:
            if len(s) == 0:
                continue
            self.sack_set.add(int(s))

    def get_data_packet(self, seq):
        self.infile.seek((seq - 1) * self.chunk_size)
        data = self.infile.read(self.chunk_size)
        if data == "":
            return None
        dat = self.make_packet("dat", seq, data)
        return dat

    def get_seq_from_ack(self, ack):
        seq = self.split_packet(ack)[1]
        if self.sackMode:
            seq = seq.split(';')[0]
        return int(seq)


'''
This will be run if you run this script from the command line. You should not
change any of this; the grader may rely on the behavior here to test your
submission.
'''
if __name__ == "__main__":
    def usage():
        print "BEARS-TP Sender"
        print "-f FILE | --file=FILE The file to transfer; if empty reads from STDIN"
        print "-p PORT | --port=PORT The destination port, defaults to 33122"
        print "-a ADDRESS | --address=ADDRESS The receiver address or hostname, defaults to localhost"
        print "-d | --debug Print debug messages"
        print "-h | --help Print this usage message"
        print "-k | --sack Enable selective acknowledgement mode"

    try:
        opts, args = getopt.getopt(sys.argv[1:],
                               "f:p:a:dk", ["file=", "port=", "address=", "debug=", "sack="])
    except:
        usage()
        exit()

    port = 33122
    dest = "localhost"
    filename = None
    debug = False
    sackMode = False

    for o,a in opts:
        if o in ("-f", "--file="):
            filename = a
        elif o in ("-p", "--port="):
            port = int(a)
        elif o in ("-a", "--address="):
            dest = a
        elif o in ("-d", "--debug="):
            debug = True
        elif o in ("-k", "--sack="):
            sackMode = True

    s = Sender(dest,port,filename,debug, sackMode)
    try:
        s.start()
    except (KeyboardInterrupt, SystemExit):
        exit()
